
noise = randn(1e5, 1);

blksize = [100, 500, 1000, 2000, 4000, 5000];

figure(1);
clf

for k=blksize
  tmp = reshape(noise, k, length(noise)/k);

  stime = mean(tmp);

  plot(k*ones(size(stime)), stime, '.');
  hold on
end

n=50:6000;
for k=1:3
  plot(n, k*sqrt(1 ./ n), '--k')
  plot(n, -k*sqrt(1 ./ n), '--k')
end

num = [1 -1];

r=0.9;
den = conv([1, 0.7], [1, -2*cos(pi/3)*r, r^2]);

figure(2)
clf

[h, w]=freqz(num, den);
w=w/(2*pi);
subplot(1,2,1);
zplane(num,den)
subplot(1,2,2)
plot(w, abs(h))

print filtro.png

filtrato = filter(num, den, noise);

figure(3)
clf

blksize=[1000, 2000, 4000, 8000];

for k=1:length(blksize)
  subplot(2,2,k)

  len=blksize(k);
  dati=filtrato(1:len);
  stima = (1/len) * abs(fft(dati)).^2;

  freq=(0:(len/2-1))/len;

  plot(freq, stima(1:len/2), w, abs(h).^2)
end

print grezzo.png

figure(4)
clf

testa = filtrato(1:max(10*blksize));

blksize=length(testa) ./ [10, 100, 200, 400];

for k=1:length(blksize)
  subplot(2,2,k)

  len=blksize(k);

  dati=reshape(testa, len, length(testa)/len);
  
  stima = (1/len) * abs(fft(dati)).^2;

  stima = mean(stima');

  freq=(0:(len/2-1))/len;

  plot(freq, stima(1:len/2), w, abs(h).^2)
end

print con-media.png


figure(5)
clf

testa = filtrato(1:8000);

blksize=length(testa) ./ [10, 100, 200, 400];

for k=1:length(blksize)
  subplot(2,2,k)

  len=blksize(k);

  dati=reshape(testa, len, length(testa)/len);
  
  stima = (1/len) * abs(fft(dati, 2^16)).^2;

  stima = mean(stima');

  L=length(stima);
  freq=(0:(L/2-1))/L;

  plot(freq, stima(1:L/2), w, abs(h).^2)
end

print con-media-corto.png


